﻿Option Explicit On
Option Strict On
'Name: Mainmenu.vb
'Description: Form for Room
'Author: Dao Ngoc Minh s3596895
'Date: 21/11/2017
'This code is written by Dao Ngoc Minh s3596895
Imports System.Drawing
Imports System.IO
Public Class Room

    'Check validation
    Private Function validateFormData() As Boolean
        Dim tt As New ToolTip()
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean = True

        'Check Room Number
        bIsValid = IsNumeric(txtRoomNumber.Text)
        If bIsValid Then
            picErrorField1.Visible = False
        Else
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "Please enter room number")
            bAllFieldsValid = False
        End If

        'Check room type
        bIsValid = oValidation.istext(txtType.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "Room type is text only")
            bAllFieldsValid = False
        End If

        'Check price
        bIsValid = IsNumeric(txtPrice.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Please enter numerical price")
            bAllFieldsValid = False
        End If

        'check number of bed
        bIsValid = IsNumeric(txtNumberOfBeds.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Please enter number of bed")
            bAllFieldsValid = False
        End If

        'check Availability
        If radiobtnAvailable.Checked = False And radiobtnUnavailable.Checked = False Then
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please choose availability")
            bAllFieldsValid = False
        Else picErrorField5.Visible = False
        End If

        'check floor
        bIsValid = IsNumeric(txtFloor.Text)
        If bIsValid Then
            picErrorField6.Visible = False
        Else
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField6, "Please enter floor number")
            bAllFieldsValid = False
        End If

        'check description
        bIsValid = oValidation.IsAlphaNumeric(txtDescription.Text)
        If bIsValid Then
            picErrorField7.Visible = False
        Else
            picErrorField7.Visible = True
            tt.SetToolTip(picErrorField7, "Please enter description")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid
    End Function

    'insert data
    Private Sub btnInsert_ClickRoom(sender As Object, e As EventArgs) Handles btnInsert.Click
        Dim bIsValid = validateFormData()
        If bIsValid Then
            Dim htData As Hashtable = New Hashtable

            'take data from textbox
            htData("room_number") = txtRoomNumber.Text
            htData("type") = txtType.Text
            htData("price") = txtPrice.Text
            htData("num_bed") = txtNumberOfBeds.Text
            htData("floor") = txtFloor.Text
            htData("description") = txtDescription.Text

            If radiobtnAvailable.Checked = True Then
                htData("availability") = True
            ElseIf radiobtnUnavailable.Checked = True Then
                htData("availability") = False
            End If

            Dim oProductController As DataController = New DataController
            Dim iNumRows = oProductController.addroom(htData)
            If iNumRows = 1 Then
                MsgBox("The room information is recorded.")
            End If
        End If
    End Sub

    'change between form
    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        Me.Close()
    End Sub

End Class