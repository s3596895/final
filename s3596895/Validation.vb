﻿Option Explicit On
Option Strict On
Imports System.Text.RegularExpressions
' Name: Validation.vb' 
'Description: Class containing validation methods
' Author: Dao Ngoc Minh s3596895
' Date: 21/11/2017
Public Class Validation

    'check number only
    Public Function IsNumericVal(ByVal strVal As String) As Boolean
        ' check if a string is numerical and return true false value
        If strVal.Length = 0 Then Return False
        Try
            Return IsNumeric(strVal)
        Catch ex As Exception
            MsgBox("error" & ex.Message)
            Return False
        End Try
    End Function

    'Check alphanumeric
    Public Function IsAlphaNumeric(ByVal strVal As String) As Boolean
        'create pattern
        Dim pattern As Regex = New Regex("[^a-zA-Z0-9 -_]")
        'check pattern
        If strVal.Length > 0 And strVal.Length < 255 Then
            Return Not pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function

    'check text only
    Public Function istext(ByVal strVal As String) As Boolean
        'create pattern for text only
        Dim pattern As Regex = New Regex("[^a-zA-Z ]")
        'check pattern
        If strVal.Length > 0 And strVal.Length < 255 Then
            Return Not pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function

    'Check email
    Public Function isvalidateEmail(ByVal strVal As String) As Boolean
        Return Regex.IsMatch(strVal, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
    End Function
End Class

