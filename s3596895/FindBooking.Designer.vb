﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FindBooking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FindBooking))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.picErrorFindValue = New System.Windows.Forms.PictureBox()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.txtFindValue = New System.Windows.Forms.TextBox()
        Me.cbbFindOption = New System.Windows.Forms.ComboBox()
        Me.lbltext = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.txtRoomPrice = New System.Windows.Forms.TextBox()
        Me.txtRoomType = New System.Windows.Forms.TextBox()
        Me.txtCustomerName = New System.Windows.Forms.TextBox()
        Me.txtComment = New System.Windows.Forms.TextBox()
        Me.txtNumberGuests = New System.Windows.Forms.TextBox()
        Me.lblComment = New System.Windows.Forms.Label()
        Me.lblTotalPrice = New System.Windows.Forms.Label()
        Me.lblCheckinDate = New System.Windows.Forms.Label()
        Me.lblNumberGuests = New System.Windows.Forms.Label()
        Me.lblNumberDays = New System.Windows.Forms.Label()
        Me.lblRoom = New System.Windows.Forms.Label()
        Me.lblCustomerID = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.btncal = New System.Windows.Forms.Button()
        Me.cbbRoomID = New System.Windows.Forms.ComboBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.cbbCustomerID = New System.Windows.Forms.ComboBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.txtTotalPrice = New System.Windows.Forms.TextBox()
        Me.CheckinDate = New System.Windows.Forms.DateTimePicker()
        Me.txtNumberDays = New System.Windows.Forms.TextBox()
        Me.txtCheckinDate = New System.Windows.Forms.TextBox()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvSearchResult = New System.Windows.Forms.DataGridView()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnfindbooking = New System.Windows.Forms.Button()
        Me.txtfindbookingvalue = New System.Windows.Forms.TextBox()
        Me.cbbFind = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.btnreport = New System.Windows.Forms.Button()
        CType(Me.picErrorFindValue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(-329, -2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 23)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "Search result:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picErrorFindValue
        '
        Me.picErrorFindValue.Location = New System.Drawing.Point(32, -40)
        Me.picErrorFindValue.Name = "picErrorFindValue"
        Me.picErrorFindValue.Size = New System.Drawing.Size(20, 20)
        Me.picErrorFindValue.TabIndex = 110
        Me.picErrorFindValue.TabStop = False
        Me.picErrorFindValue.Visible = False
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(58, -41)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(75, 21)
        Me.btnFind.TabIndex = 109
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'txtFindValue
        '
        Me.txtFindValue.Location = New System.Drawing.Point(-192, -42)
        Me.txtFindValue.Name = "txtFindValue"
        Me.txtFindValue.Size = New System.Drawing.Size(218, 20)
        Me.txtFindValue.TabIndex = 108
        '
        'cbbFindOption
        '
        Me.cbbFindOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFindOption.Location = New System.Drawing.Point(-323, -41)
        Me.cbbFindOption.Name = "cbbFindOption"
        Me.cbbFindOption.Size = New System.Drawing.Size(125, 21)
        Me.cbbFindOption.TabIndex = 107
        '
        'lbltext
        '
        Me.lbltext.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.lbltext.Location = New System.Drawing.Point(-370, -50)
        Me.lbltext.Name = "lbltext"
        Me.lbltext.Size = New System.Drawing.Size(41, 32)
        Me.lbltext.TabIndex = 106
        Me.lbltext.Text = "by"
        Me.lbltext.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(-226, -96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(235, 49)
        Me.Label1.TabIndex = 105
        Me.Label1.Text = "Find booking"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(229, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 23)
        Me.Label3.TabIndex = 122
        Me.Label3.Text = "Search result:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Enabled = False
        Me.txtRoomNumber.Location = New System.Drawing.Point(744, 177)
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(60, 20)
        Me.txtRoomNumber.TabIndex = 136
        '
        'txtRoomPrice
        '
        Me.txtRoomPrice.Enabled = False
        Me.txtRoomPrice.Location = New System.Drawing.Point(336, 65)
        Me.txtRoomPrice.Name = "txtRoomPrice"
        Me.txtRoomPrice.Size = New System.Drawing.Size(106, 20)
        Me.txtRoomPrice.TabIndex = 135
        '
        'txtRoomType
        '
        Me.txtRoomType.Enabled = False
        Me.txtRoomType.Location = New System.Drawing.Point(810, 177)
        Me.txtRoomType.Name = "txtRoomType"
        Me.txtRoomType.Size = New System.Drawing.Size(60, 20)
        Me.txtRoomType.TabIndex = 134
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Enabled = False
        Me.txtCustomerName.Location = New System.Drawing.Point(744, 146)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(240, 20)
        Me.txtCustomerName.TabIndex = 133
        '
        'txtComment
        '
        Me.txtComment.Enabled = False
        Me.txtComment.Location = New System.Drawing.Point(672, 333)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(312, 20)
        Me.txtComment.TabIndex = 132
        '
        'txtNumberGuests
        '
        Me.txtNumberGuests.Enabled = False
        Me.txtNumberGuests.Location = New System.Drawing.Point(672, 240)
        Me.txtNumberGuests.Name = "txtNumberGuests"
        Me.txtNumberGuests.Size = New System.Drawing.Size(100, 20)
        Me.txtNumberGuests.TabIndex = 130
        '
        'lblComment
        '
        Me.lblComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblComment.Location = New System.Drawing.Point(559, 333)
        Me.lblComment.Name = "lblComment"
        Me.lblComment.Size = New System.Drawing.Size(98, 20)
        Me.lblComment.TabIndex = 129
        Me.lblComment.Text = "Comment"
        Me.lblComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPrice
        '
        Me.lblTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotalPrice.Location = New System.Drawing.Point(559, 301)
        Me.lblTotalPrice.Name = "lblTotalPrice"
        Me.lblTotalPrice.Size = New System.Drawing.Size(98, 20)
        Me.lblTotalPrice.TabIndex = 128
        Me.lblTotalPrice.Text = "Total price"
        Me.lblTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCheckinDate
        '
        Me.lblCheckinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCheckinDate.Location = New System.Drawing.Point(559, 269)
        Me.lblCheckinDate.Name = "lblCheckinDate"
        Me.lblCheckinDate.Size = New System.Drawing.Size(98, 20)
        Me.lblCheckinDate.TabIndex = 127
        Me.lblCheckinDate.Text = "Check in Date"
        Me.lblCheckinDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumberGuests
        '
        Me.lblNumberGuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberGuests.Location = New System.Drawing.Point(559, 239)
        Me.lblNumberGuests.Name = "lblNumberGuests"
        Me.lblNumberGuests.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberGuests.TabIndex = 126
        Me.lblNumberGuests.Text = "Number of guests"
        Me.lblNumberGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumberDays
        '
        Me.lblNumberDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNumberDays.Location = New System.Drawing.Point(559, 208)
        Me.lblNumberDays.Name = "lblNumberDays"
        Me.lblNumberDays.Size = New System.Drawing.Size(98, 20)
        Me.lblNumberDays.TabIndex = 125
        Me.lblNumberDays.Text = "Number of days"
        Me.lblNumberDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRoom
        '
        Me.lblRoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRoom.Location = New System.Drawing.Point(559, 176)
        Me.lblRoom.Name = "lblRoom"
        Me.lblRoom.Size = New System.Drawing.Size(98, 20)
        Me.lblRoom.TabIndex = 124
        Me.lblRoom.Text = "Room"
        Me.lblRoom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCustomerID
        '
        Me.lblCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomerID.Location = New System.Drawing.Point(559, 147)
        Me.lblCustomerID.Name = "lblCustomerID"
        Me.lblCustomerID.Size = New System.Drawing.Size(98, 20)
        Me.lblCustomerID.TabIndex = 123
        Me.lblCustomerID.Text = "Customer"
        Me.lblCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnreport)
        Me.GroupBox1.Controls.Add(Me.PictureBox6)
        Me.GroupBox1.Controls.Add(Me.btncal)
        Me.GroupBox1.Controls.Add(Me.cbbRoomID)
        Me.GroupBox1.Controls.Add(Me.PictureBox5)
        Me.GroupBox1.Controls.Add(Me.cbbCustomerID)
        Me.GroupBox1.Controls.Add(Me.PictureBox4)
        Me.GroupBox1.Controls.Add(Me.PictureBox3)
        Me.GroupBox1.Controls.Add(Me.txtTotalPrice)
        Me.GroupBox1.Controls.Add(Me.CheckinDate)
        Me.GroupBox1.Controls.Add(Me.txtNumberDays)
        Me.GroupBox1.Controls.Add(Me.txtCheckinDate)
        Me.GroupBox1.Controls.Add(Me.btntDelete)
        Me.GroupBox1.Controls.Add(Me.txtRoomPrice)
        Me.GroupBox1.Controls.Add(Me.btnNext)
        Me.GroupBox1.Controls.Add(Me.btnEdit)
        Me.GroupBox1.Controls.Add(Me.btnPrev)
        Me.GroupBox1.Location = New System.Drawing.Point(542, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 302)
        Me.GroupBox1.TabIndex = 139
        Me.GroupBox1.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(462, 221)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox6.TabIndex = 148
        Me.PictureBox6.TabStop = False
        Me.PictureBox6.Visible = False
        '
        'btncal
        '
        Me.btncal.Location = New System.Drawing.Point(367, 189)
        Me.btncal.Name = "btncal"
        Me.btncal.Size = New System.Drawing.Size(75, 20)
        Me.btncal.TabIndex = 147
        Me.btncal.Text = "Calc"
        Me.btncal.UseVisualStyleBackColor = True
        '
        'cbbRoomID
        '
        Me.cbbRoomID.Enabled = False
        Me.cbbRoomID.FormattingEnabled = True
        Me.cbbRoomID.Location = New System.Drawing.Point(130, 64)
        Me.cbbRoomID.Name = "cbbRoomID"
        Me.cbbRoomID.Size = New System.Drawing.Size(66, 21)
        Me.cbbRoomID.TabIndex = 142
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(461, 190)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox5.TabIndex = 146
        Me.PictureBox5.TabStop = False
        Me.PictureBox5.Visible = False
        '
        'cbbCustomerID
        '
        Me.cbbCustomerID.Enabled = False
        Me.cbbCustomerID.FormattingEnabled = True
        Me.cbbCustomerID.Location = New System.Drawing.Point(130, 35)
        Me.cbbCustomerID.Name = "cbbCustomerID"
        Me.cbbCustomerID.Size = New System.Drawing.Size(66, 21)
        Me.cbbCustomerID.TabIndex = 141
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(461, 128)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox4.TabIndex = 145
        Me.PictureBox4.TabStop = False
        Me.PictureBox4.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(461, 96)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(20, 20)
        Me.PictureBox3.TabIndex = 144
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'txtTotalPrice
        '
        Me.txtTotalPrice.Enabled = False
        Me.txtTotalPrice.Location = New System.Drawing.Point(130, 190)
        Me.txtTotalPrice.Name = "txtTotalPrice"
        Me.txtTotalPrice.Size = New System.Drawing.Size(231, 20)
        Me.txtTotalPrice.TabIndex = 141
        '
        'CheckinDate
        '
        Me.CheckinDate.Location = New System.Drawing.Point(130, 157)
        Me.CheckinDate.Name = "CheckinDate"
        Me.CheckinDate.Size = New System.Drawing.Size(200, 20)
        Me.CheckinDate.TabIndex = 88
        Me.CheckinDate.Visible = False
        '
        'txtNumberDays
        '
        Me.txtNumberDays.Enabled = False
        Me.txtNumberDays.Location = New System.Drawing.Point(130, 96)
        Me.txtNumberDays.Name = "txtNumberDays"
        Me.txtNumberDays.Size = New System.Drawing.Size(100, 20)
        Me.txtNumberDays.TabIndex = 87
        '
        'txtCheckinDate
        '
        Me.txtCheckinDate.Enabled = False
        Me.txtCheckinDate.Location = New System.Drawing.Point(130, 157)
        Me.txtCheckinDate.Name = "txtCheckinDate"
        Me.txtCheckinDate.Size = New System.Drawing.Size(304, 20)
        Me.txtCheckinDate.TabIndex = 87
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(268, 5)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(75, 23)
        Me.btntDelete.TabIndex = 59
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(260, 268)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 82
        Me.btnNext.Text = "Next record"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(371, 5)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 58
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(118, 268)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 23)
        Me.btnPrev.TabIndex = 81
        Me.btnPrev.Text = "Previous record"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvSearchResult)
        Me.Panel1.Location = New System.Drawing.Point(34, 112)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 302)
        Me.Panel1.TabIndex = 121
        '
        'dgvSearchResult
        '
        Me.dgvSearchResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchResult.Location = New System.Drawing.Point(0, 14)
        Me.dgvSearchResult.Name = "dgvSearchResult"
        Me.dgvSearchResult.ReadOnly = True
        Me.dgvSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSearchResult.Size = New System.Drawing.Size(474, 285)
        Me.dgvSearchResult.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(473, 429)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 30)
        Me.btnClose.TabIndex = 120
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnfindbooking
        '
        Me.btnfindbooking.Location = New System.Drawing.Point(482, 59)
        Me.btnfindbooking.Name = "btnfindbooking"
        Me.btnfindbooking.Size = New System.Drawing.Size(75, 21)
        Me.btnfindbooking.TabIndex = 118
        Me.btnfindbooking.Text = "Find"
        Me.btnfindbooking.UseVisualStyleBackColor = True
        '
        'txtfindbookingvalue
        '
        Me.txtfindbookingvalue.Enabled = False
        Me.txtfindbookingvalue.Location = New System.Drawing.Point(232, 58)
        Me.txtfindbookingvalue.Name = "txtfindbookingvalue"
        Me.txtfindbookingvalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindbookingvalue.TabIndex = 117
        '
        'cbbFind
        '
        Me.cbbFind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFind.Location = New System.Drawing.Point(101, 59)
        Me.cbbFind.Name = "cbbFind"
        Me.cbbFind.Size = New System.Drawing.Size(125, 21)
        Me.cbbFind.TabIndex = 116
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label4.Location = New System.Drawing.Point(5, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 32)
        Me.Label4.TabIndex = 115
        Me.Label4.Text = "Find"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 30.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(473, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(235, 49)
        Me.Label5.TabIndex = 114
        Me.Label5.Text = "Find booking"
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(456, 59)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 140
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'btnreport
        '
        Me.btnreport.Location = New System.Drawing.Point(367, 268)
        Me.btnreport.Name = "btnreport"
        Me.btnreport.Size = New System.Drawing.Size(75, 23)
        Me.btnreport.TabIndex = 149
        Me.btnreport.Text = "report"
        Me.btnreport.UseVisualStyleBackColor = True
        '
        'FindBooking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1035, 465)
        Me.Controls.Add(Me.picErrorField)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtRoomNumber)
        Me.Controls.Add(Me.txtRoomType)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.txtComment)
        Me.Controls.Add(Me.txtNumberGuests)
        Me.Controls.Add(Me.lblComment)
        Me.Controls.Add(Me.lblTotalPrice)
        Me.Controls.Add(Me.lblCheckinDate)
        Me.Controls.Add(Me.lblNumberGuests)
        Me.Controls.Add(Me.lblNumberDays)
        Me.Controls.Add(Me.lblRoom)
        Me.Controls.Add(Me.lblCustomerID)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnfindbooking)
        Me.Controls.Add(Me.txtfindbookingvalue)
        Me.Controls.Add(Me.cbbFind)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.picErrorFindValue)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.txtFindValue)
        Me.Controls.Add(Me.cbbFindOption)
        Me.Controls.Add(Me.lbltext)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FindBooking"
        Me.Text = "FindBooking"
        CType(Me.picErrorFindValue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents picErrorFindValue As PictureBox
    Friend WithEvents btnFind As Button
    Friend WithEvents txtFindValue As TextBox
    Friend WithEvents cbbFindOption As ComboBox
    Friend WithEvents lbltext As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents txtRoomPrice As TextBox
    Friend WithEvents txtRoomType As TextBox
    Friend WithEvents txtCustomerName As TextBox
    Friend WithEvents txtComment As TextBox
    Friend WithEvents txtNumberGuests As TextBox
    Friend WithEvents lblComment As Label
    Friend WithEvents lblTotalPrice As Label
    Friend WithEvents lblCheckinDate As Label
    Friend WithEvents lblNumberGuests As Label
    Friend WithEvents lblNumberDays As Label
    Friend WithEvents lblRoom As Label
    Friend WithEvents lblCustomerID As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents CheckinDate As DateTimePicker
    Friend WithEvents txtNumberDays As TextBox
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnPrev As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dgvSearchResult As DataGridView
    Friend WithEvents btnClose As Button
    Friend WithEvents btnfindbooking As Button
    Friend WithEvents txtfindbookingvalue As TextBox
    Friend WithEvents cbbFind As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents picErrorField As PictureBox
    Friend WithEvents txtTotalPrice As TextBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents cbbRoomID As ComboBox
    Friend WithEvents cbbCustomerID As ComboBox
    Friend WithEvents btncal As Button
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents txtCheckinDate As TextBox
    Friend WithEvents btnreport As Button
End Class
