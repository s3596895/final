﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Customer))
        Me.DobValue = New System.Windows.Forms.DateTimePicker()
        Me.radiobtnFemale = New System.Windows.Forms.RadioButton()
        Me.radiobtnMale = New System.Windows.Forms.RadioButton()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.lblcusLastName = New System.Windows.Forms.Label()
        Me.lblcusPhone = New System.Windows.Forms.Label()
        Me.lblcusAddress = New System.Windows.Forms.Label()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.lblcusEmail = New System.Windows.Forms.Label()
        Me.btnMenu = New System.Windows.Forms.Button()
        Me.lblcusDob = New System.Windows.Forms.Label()
        Me.lblcusFirstName = New System.Windows.Forms.Label()
        Me.lblcusGender = New System.Windows.Forms.Label()
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.gbxCustomer = New System.Windows.Forms.GroupBox()
        Me.txttitle = New System.Windows.Forms.TextBox()
        Me.lbltitle = New System.Windows.Forms.Label()
        Me.txtcustomerid = New System.Windows.Forms.TextBox()
        Me.lblcustomerid = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnclear = New System.Windows.Forms.Button()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCustomer.SuspendLayout()
        Me.SuspendLayout()
        '
        'DobValue
        '
        Me.DobValue.Location = New System.Drawing.Point(80, 200)
        Me.DobValue.Name = "DobValue"
        Me.DobValue.Size = New System.Drawing.Size(258, 20)
        Me.DobValue.TabIndex = 6
        Me.DobValue.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'radiobtnFemale
        '
        Me.radiobtnFemale.AutoSize = True
        Me.radiobtnFemale.Location = New System.Drawing.Point(279, 22)
        Me.radiobtnFemale.Name = "radiobtnFemale"
        Me.radiobtnFemale.Size = New System.Drawing.Size(59, 17)
        Me.radiobtnFemale.TabIndex = 6
        Me.radiobtnFemale.Text = "Female"
        Me.radiobtnFemale.UseVisualStyleBackColor = True
        '
        'radiobtnMale
        '
        Me.radiobtnMale.AutoSize = True
        Me.radiobtnMale.Location = New System.Drawing.Point(80, 22)
        Me.radiobtnMale.Name = "radiobtnMale"
        Me.radiobtnMale.Size = New System.Drawing.Size(48, 17)
        Me.radiobtnMale.TabIndex = 5
        Me.radiobtnMale.Text = "Male"
        Me.radiobtnMale.UseVisualStyleBackColor = True
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(80, 169)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(258, 20)
        Me.txtEmail.TabIndex = 5
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(80, 140)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(258, 20)
        Me.txtAddress.TabIndex = 4
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(80, 51)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(258, 20)
        Me.txtFirstName.TabIndex = 1
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(80, 80)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(258, 20)
        Me.txtLastName.TabIndex = 2
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(361, 121)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 18
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(362, 92)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 17
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(362, 61)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 16
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(362, 151)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 15
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'lblcusLastName
        '
        Me.lblcusLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusLastName.Location = New System.Drawing.Point(6, 80)
        Me.lblcusLastName.Name = "lblcusLastName"
        Me.lblcusLastName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusLastName.TabIndex = 7
        Me.lblcusLastName.Text = "Last Name"
        Me.lblcusLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusPhone
        '
        Me.lblcusPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusPhone.Location = New System.Drawing.Point(6, 110)
        Me.lblcusPhone.Name = "lblcusPhone"
        Me.lblcusPhone.Size = New System.Drawing.Size(68, 20)
        Me.lblcusPhone.TabIndex = 9
        Me.lblcusPhone.Text = "Phone"
        Me.lblcusPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusAddress
        '
        Me.lblcusAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusAddress.Location = New System.Drawing.Point(6, 140)
        Me.lblcusAddress.Name = "lblcusAddress"
        Me.lblcusAddress.Size = New System.Drawing.Size(68, 20)
        Me.lblcusAddress.TabIndex = 11
        Me.lblcusAddress.Text = "Address"
        Me.lblcusAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(361, 181)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 19
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'lblcusEmail
        '
        Me.lblcusEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusEmail.Location = New System.Drawing.Point(6, 169)
        Me.lblcusEmail.Name = "lblcusEmail"
        Me.lblcusEmail.Size = New System.Drawing.Size(68, 20)
        Me.lblcusEmail.TabIndex = 13
        Me.lblcusEmail.Text = "Email"
        Me.lblcusEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnMenu
        '
        Me.btnMenu.Location = New System.Drawing.Point(394, 63)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(115, 23)
        Me.btnMenu.TabIndex = 21
        Me.btnMenu.Text = "Booking"
        Me.btnMenu.UseVisualStyleBackColor = True
        '
        'lblcusDob
        '
        Me.lblcusDob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusDob.Location = New System.Drawing.Point(5, 200)
        Me.lblcusDob.Name = "lblcusDob"
        Me.lblcusDob.Size = New System.Drawing.Size(68, 20)
        Me.lblcusDob.TabIndex = 15
        Me.lblcusDob.Text = "Dob"
        Me.lblcusDob.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusFirstName
        '
        Me.lblcusFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusFirstName.Location = New System.Drawing.Point(6, 51)
        Me.lblcusFirstName.Name = "lblcusFirstName"
        Me.lblcusFirstName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusFirstName.TabIndex = 5
        Me.lblcusFirstName.Text = "First Name"
        Me.lblcusFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusGender
        '
        Me.lblcusGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusGender.Location = New System.Drawing.Point(6, 20)
        Me.lblcusGender.Name = "lblcusGender"
        Me.lblcusGender.Size = New System.Drawing.Size(68, 20)
        Me.lblcusGender.TabIndex = 2
        Me.lblcusGender.Text = "Gender"
        Me.lblcusGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(362, 31)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 14
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(394, 30)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(115, 23)
        Me.btnInsert.TabIndex = 13
        Me.btnInsert.Text = "Insert customer"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'gbxCustomer
        '
        Me.gbxCustomer.Controls.Add(Me.txttitle)
        Me.gbxCustomer.Controls.Add(Me.lbltitle)
        Me.gbxCustomer.Controls.Add(Me.txtcustomerid)
        Me.gbxCustomer.Controls.Add(Me.lblcustomerid)
        Me.gbxCustomer.Controls.Add(Me.txtPhone)
        Me.gbxCustomer.Controls.Add(Me.DobValue)
        Me.gbxCustomer.Controls.Add(Me.radiobtnFemale)
        Me.gbxCustomer.Controls.Add(Me.radiobtnMale)
        Me.gbxCustomer.Controls.Add(Me.txtEmail)
        Me.gbxCustomer.Controls.Add(Me.txtAddress)
        Me.gbxCustomer.Controls.Add(Me.txtFirstName)
        Me.gbxCustomer.Controls.Add(Me.txtLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusPhone)
        Me.gbxCustomer.Controls.Add(Me.lblcusAddress)
        Me.gbxCustomer.Controls.Add(Me.lblcusEmail)
        Me.gbxCustomer.Controls.Add(Me.lblcusDob)
        Me.gbxCustomer.Controls.Add(Me.lblcusFirstName)
        Me.gbxCustomer.Controls.Add(Me.lblcusGender)
        Me.gbxCustomer.Location = New System.Drawing.Point(10, 11)
        Me.gbxCustomer.Name = "gbxCustomer"
        Me.gbxCustomer.Size = New System.Drawing.Size(345, 299)
        Me.gbxCustomer.TabIndex = 12
        Me.gbxCustomer.TabStop = False
        Me.gbxCustomer.Text = "Customer Details"
        '
        'txttitle
        '
        Me.txttitle.Enabled = False
        Me.txttitle.Location = New System.Drawing.Point(80, 261)
        Me.txttitle.Name = "txttitle"
        Me.txttitle.Size = New System.Drawing.Size(258, 20)
        Me.txttitle.TabIndex = 19
        '
        'lbltitle
        '
        Me.lbltitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbltitle.Location = New System.Drawing.Point(6, 260)
        Me.lbltitle.Name = "lbltitle"
        Me.lbltitle.Size = New System.Drawing.Size(68, 20)
        Me.lbltitle.TabIndex = 18
        Me.lbltitle.Text = "Title"
        Me.lbltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcustomerid
        '
        Me.txtcustomerid.Enabled = False
        Me.txtcustomerid.Location = New System.Drawing.Point(81, 229)
        Me.txtcustomerid.Name = "txtcustomerid"
        Me.txtcustomerid.Size = New System.Drawing.Size(258, 20)
        Me.txtcustomerid.TabIndex = 17
        '
        'lblcustomerid
        '
        Me.lblcustomerid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcustomerid.Location = New System.Drawing.Point(5, 230)
        Me.lblcustomerid.Name = "lblcustomerid"
        Me.lblcustomerid.Size = New System.Drawing.Size(68, 20)
        Me.lblcustomerid.TabIndex = 16
        Me.lblcustomerid.Text = "Customer ID"
        Me.lblcustomerid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(81, 109)
        Me.txtPhone.MaxLength = 9
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(257, 20)
        Me.txtPhone.TabIndex = 3
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(394, 92)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(115, 23)
        Me.btnFind.TabIndex = 22
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Location = New System.Drawing.Point(394, 121)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(115, 23)
        Me.btnupdate.TabIndex = 23
        Me.btnupdate.Text = "update"
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Location = New System.Drawing.Point(394, 151)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(115, 23)
        Me.btndelete.TabIndex = 24
        Me.btndelete.Text = "Delete"
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnclear
        '
        Me.btnclear.Location = New System.Drawing.Point(394, 181)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(115, 23)
        Me.btnclear.TabIndex = 25
        Me.btnclear.Text = "Clear"
        Me.btnclear.UseVisualStyleBackColor = True
        '
        'Customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 361)
        Me.Controls.Add(Me.btnclear)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.btnupdate)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.btnMenu)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.gbxCustomer)
        Me.Name = "Customer"
        Me.Text = "Customer"
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCustomer.ResumeLayout(False)
        Me.gbxCustomer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DobValue As DateTimePicker
    Friend WithEvents radiobtnFemale As RadioButton
    Friend WithEvents radiobtnMale As RadioButton
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents lblcusLastName As Label
    Friend WithEvents lblcusPhone As Label
    Friend WithEvents lblcusAddress As Label
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents lblcusEmail As Label
    Friend WithEvents btnMenu As Button
    Friend WithEvents lblcusDob As Label
    Friend WithEvents lblcusFirstName As Label
    Friend WithEvents lblcusGender As Label
    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents btnInsert As Button
    Friend WithEvents gbxCustomer As GroupBox
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents btnFind As Button
    Friend WithEvents txtcustomerid As TextBox
    Friend WithEvents lblcustomerid As Label
    Friend WithEvents txttitle As TextBox
    Friend WithEvents lbltitle As Label
    Friend WithEvents btnupdate As Button
    Friend WithEvents btndelete As Button
    Friend WithEvents btnclear As Button
End Class
