﻿Option Explicit On
Option Strict On
'Name: Mainmenu.vb
'Description: Form for Customer
'Author: Dao Ngoc Minh s3596895
'Date: 21/11/2017
'This code is written by Dao Ngoc Minh s3596895
Imports System.Drawing
Imports System.IO
Public Class Customer

    'Check validation
    Private Function validateFormData() As Boolean
        Dim tt As New ToolTip()
        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean = True

        'Validate gender
        If radiobtnMale.Checked = False And radiobtnFemale.Checked = False Then
            picErrorField1.Visible = True
            tt.SetToolTip(picErrorField1, "Please choose your gender")
            bAllFieldsValid = False
        Else picErrorField1.Visible = False
        End If

        'Validate first name - only text
        bIsValid = oValidation.istext(txtFirstName.Text)
        If bIsValid Then
            picErrorField2.Visible = False
        Else
            picErrorField2.Visible = True
            tt.SetToolTip(picErrorField2, "First name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate last name - only text
        bIsValid = oValidation.istext(txtLastName.Text)
        If bIsValid Then
            picErrorField3.Visible = False
        Else
            picErrorField3.Visible = True
            tt.SetToolTip(picErrorField3, "Last name is empty or contain numbers")
            bAllFieldsValid = False
        End If

        'Validate phone number - only number        
        bIsValid = IsNumeric(txtPhone.Text)
        If bIsValid Then
            picErrorField4.Visible = False
        Else
            picErrorField4.Visible = True
            tt.SetToolTip(picErrorField4, "Characters and blank space are not allow")
            bAllFieldsValid = False
        End If

        'Validate address - no empty
        bIsValid = oValidation.IsAlphaNumeric(txtAddress.Text)
        If bIsValid Then
            picErrorField5.Visible = False
        Else
            picErrorField5.Visible = True
            tt.SetToolTip(picErrorField5, "Please enter your address")
            bAllFieldsValid = False
        End If

        'Validate email - email format only
        bIsValid = oValidation.isvalidateEmail(txtEmail.Text)
        If bIsValid Then
            picErrorField6.Visible = False
        Else
            picErrorField6.Visible = True
            tt.SetToolTip(picErrorField6, "Please enter correct email format")
            bAllFieldsValid = False
        End If

        'check all field
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        End If
        Return bAllFieldsValid
    End Function

    Private Sub btnInsert_ClickCustomer(sender As Object, e As EventArgs) Handles btnInsert.Click
        Dim bIsValid = validateFormData()
        If bIsValid Then
            Dim htData As Hashtable = New Hashtable

            'take data from textbox
            If radiobtnMale.Checked Then
                htData("title") = "Mr"
            ElseIf radiobtnFemale.Checked Then
                htData("title") = "Ms"
            End If
            htData("firstname") = txtFirstName.Text
            htData("lastname") = txtLastName.Text
            htData("phone") = txtPhone.Text
            htData("address") = txtAddress.Text
            htData("email") = txtEmail.Text
            htData("dob") = DobValue.Value
            If radiobtnMale.Checked Then
                htData("gender") = "Male"
            Else
                htData("gender") = "Female"
            End If

            Dim oProductController As DataController = New DataController

            Dim iNumRows = oProductController.insertCustomer(htData)
            If iNumRows = 1 Then
                MsgBox("The customer information is recorded.")
            End If
        End If
    End Sub

    'Change between form
    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        Me.Close()
    End Sub



    'delete
    ''Private Sub btnupdate_Click(sender As Object, e As EventArgs) Handles btnupdate.Click
    'Dim oController As DataController = New DataController
    'Dim iNumRows = oController.update(getFormData)
    'If iNumRows = 1 Then
    'Debug.Print("The record was updated. Use the find button to check ...")
    'MsgBox("The record was updated. Use the find button to check ...")
    'End If
    ' End Sub

    ' Private Sub btndelete_Click(sender As Object, e As EventArgs) Handles btndelete.Click
    'Dim oController As DataController = New DataController
    'Dim deletevar = txtcustomerid.Text
    'Dim iNumRows = oController.delete(deletevar)

    'If iNumRows = 1 Then
    '    clearForm()
    '    Debug.Print("The record was deleted. Use the find button to check ...")
    '    MsgBox("The record was deleted. Use the find button to check ...")
    'Else
    '    Debug.Print("The record was not deleted!")
    '   MsgBox("The record was not deleted!")
    'End If

    ' End Sub

    'clear form
    Private Sub clearForm()

        txtcustomerid.Clear()
        txttitle.Clear()
        txtFirstName.Clear()
        txtLastName.Clear()
        txtPhone.Clear()
        txtAddress.Clear()
        txtEmail.Clear()
        radiobtnFemale.Checked = False
        radiobtnMale.Checked = False
    End Sub

    Private Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        clearForm()
    End Sub

    Private Sub Customer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class