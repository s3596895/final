﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FindRoom
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FindRoom))
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvSearchResult = New System.Windows.Forms.DataGridView()
        Me.btnfindroom = New System.Windows.Forms.Button()
        Me.txtfindroomvalue = New System.Windows.Forms.TextBox()
        Me.cbbFind = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblFindRoom = New System.Windows.Forms.Label()
        Me.gbxRoom = New System.Windows.Forms.GroupBox()
        Me.txtFloor = New System.Windows.Forms.TextBox()
        Me.txtNumberOfBeds = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.radiobtnUnavailable = New System.Windows.Forms.RadioButton()
        Me.radiobtnAvailable = New System.Windows.Forms.RadioButton()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.lblroomType = New System.Windows.Forms.Label()
        Me.lblroomPrice = New System.Windows.Forms.Label()
        Me.lblroomNumberOfBeds = New System.Windows.Forms.Label()
        Me.lblroomAvailability = New System.Windows.Forms.Label()
        Me.lblroomFloor = New System.Windows.Forms.Label()
        Me.lblroomDescription = New System.Windows.Forms.Label()
        Me.lblroomRoomNumber = New System.Windows.Forms.Label()
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.picErrorField7 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxRoom.SuspendLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(447, 426)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 30)
        Me.btnClose.TabIndex = 173
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(795, 430)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 172
        Me.btnNext.Text = "Next record"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(618, 430)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(75, 23)
        Me.btntDelete.TabIndex = 170
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(882, 430)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 169
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(699, 430)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 23)
        Me.btnPrev.TabIndex = 171
        Me.btnPrev.Text = "Previous record"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(263, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 23)
        Me.Label3.TabIndex = 167
        Me.Label3.Text = "Search result:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvSearchResult)
        Me.Panel1.Location = New System.Drawing.Point(78, 114)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 302)
        Me.Panel1.TabIndex = 166
        '
        'dgvSearchResult
        '
        Me.dgvSearchResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchResult.Location = New System.Drawing.Point(0, 14)
        Me.dgvSearchResult.Name = "dgvSearchResult"
        Me.dgvSearchResult.ReadOnly = True
        Me.dgvSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSearchResult.Size = New System.Drawing.Size(474, 285)
        Me.dgvSearchResult.TabIndex = 0
        '
        'btnfindroom
        '
        Me.btnfindroom.Location = New System.Drawing.Point(565, 61)
        Me.btnfindroom.Name = "btnfindroom"
        Me.btnfindroom.Size = New System.Drawing.Size(75, 21)
        Me.btnfindroom.TabIndex = 164
        Me.btnfindroom.Text = "Find"
        Me.btnfindroom.UseVisualStyleBackColor = True
        '
        'txtfindroomvalue
        '
        Me.txtfindroomvalue.Enabled = False
        Me.txtfindroomvalue.Location = New System.Drawing.Point(315, 60)
        Me.txtfindroomvalue.Name = "txtfindroomvalue"
        Me.txtfindroomvalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindroomvalue.TabIndex = 163
        '
        'cbbFind
        '
        Me.cbbFind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFind.Location = New System.Drawing.Point(184, 61)
        Me.cbbFind.Name = "cbbFind"
        Me.cbbFind.Size = New System.Drawing.Size(125, 21)
        Me.cbbFind.TabIndex = 162
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label4.Location = New System.Drawing.Point(88, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 32)
        Me.Label4.TabIndex = 161
        Me.Label4.Text = "Find"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFindRoom
        '
        Me.lblFindRoom.AutoSize = True
        Me.lblFindRoom.Font = New System.Drawing.Font("Calibri", 30.0!, System.Drawing.FontStyle.Bold)
        Me.lblFindRoom.Location = New System.Drawing.Point(512, 8)
        Me.lblFindRoom.Name = "lblFindRoom"
        Me.lblFindRoom.Size = New System.Drawing.Size(200, 49)
        Me.lblFindRoom.TabIndex = 160
        Me.lblFindRoom.Text = "Find Room"
        '
        'gbxRoom
        '
        Me.gbxRoom.BackColor = System.Drawing.SystemColors.ControlLight
        Me.gbxRoom.Controls.Add(Me.txtFloor)
        Me.gbxRoom.Controls.Add(Me.txtNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.txtPrice)
        Me.gbxRoom.Controls.Add(Me.txtRoomNumber)
        Me.gbxRoom.Controls.Add(Me.radiobtnUnavailable)
        Me.gbxRoom.Controls.Add(Me.radiobtnAvailable)
        Me.gbxRoom.Controls.Add(Me.txtDescription)
        Me.gbxRoom.Controls.Add(Me.txtType)
        Me.gbxRoom.Controls.Add(Me.lblroomType)
        Me.gbxRoom.Controls.Add(Me.lblroomPrice)
        Me.gbxRoom.Controls.Add(Me.lblroomNumberOfBeds)
        Me.gbxRoom.Controls.Add(Me.lblroomAvailability)
        Me.gbxRoom.Controls.Add(Me.lblroomFloor)
        Me.gbxRoom.Controls.Add(Me.lblroomDescription)
        Me.gbxRoom.Controls.Add(Me.lblroomRoomNumber)
        Me.gbxRoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxRoom.Location = New System.Drawing.Point(619, 128)
        Me.gbxRoom.Name = "gbxRoom"
        Me.gbxRoom.Size = New System.Drawing.Size(338, 254)
        Me.gbxRoom.TabIndex = 174
        Me.gbxRoom.TabStop = False
        Me.gbxRoom.Text = "Room Details"
        '
        'txtFloor
        '
        Me.txtFloor.Enabled = False
        Me.txtFloor.Location = New System.Drawing.Point(103, 168)
        Me.txtFloor.MaxLength = 2
        Me.txtFloor.Name = "txtFloor"
        Me.txtFloor.Size = New System.Drawing.Size(228, 20)
        Me.txtFloor.TabIndex = 5
        '
        'txtNumberOfBeds
        '
        Me.txtNumberOfBeds.Enabled = False
        Me.txtNumberOfBeds.Location = New System.Drawing.Point(103, 108)
        Me.txtNumberOfBeds.MaxLength = 2
        Me.txtNumberOfBeds.Name = "txtNumberOfBeds"
        Me.txtNumberOfBeds.Size = New System.Drawing.Size(228, 20)
        Me.txtNumberOfBeds.TabIndex = 4
        '
        'txtPrice
        '
        Me.txtPrice.Enabled = False
        Me.txtPrice.Location = New System.Drawing.Point(103, 78)
        Me.txtPrice.MaxLength = 7
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(228, 20)
        Me.txtPrice.TabIndex = 3
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Enabled = False
        Me.txtRoomNumber.Location = New System.Drawing.Point(103, 20)
        Me.txtRoomNumber.MaxLength = 3
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(228, 20)
        Me.txtRoomNumber.TabIndex = 1
        '
        'radiobtnUnavailable
        '
        Me.radiobtnUnavailable.AutoSize = True
        Me.radiobtnUnavailable.Enabled = False
        Me.radiobtnUnavailable.Location = New System.Drawing.Point(241, 138)
        Me.radiobtnUnavailable.Name = "radiobtnUnavailable"
        Me.radiobtnUnavailable.Size = New System.Drawing.Size(81, 17)
        Me.radiobtnUnavailable.TabIndex = 10
        Me.radiobtnUnavailable.TabStop = True
        Me.radiobtnUnavailable.Text = "Unavailable"
        Me.radiobtnUnavailable.UseVisualStyleBackColor = True
        '
        'radiobtnAvailable
        '
        Me.radiobtnAvailable.AutoSize = True
        Me.radiobtnAvailable.Enabled = False
        Me.radiobtnAvailable.Location = New System.Drawing.Point(103, 140)
        Me.radiobtnAvailable.Name = "radiobtnAvailable"
        Me.radiobtnAvailable.Size = New System.Drawing.Size(68, 17)
        Me.radiobtnAvailable.TabIndex = 9
        Me.radiobtnAvailable.TabStop = True
        Me.radiobtnAvailable.Text = "Available"
        Me.radiobtnAvailable.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.Enabled = False
        Me.txtDescription.Location = New System.Drawing.Point(103, 199)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(229, 20)
        Me.txtDescription.TabIndex = 6
        '
        'txtType
        '
        Me.txtType.Enabled = False
        Me.txtType.Location = New System.Drawing.Point(103, 51)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(229, 20)
        Me.txtType.TabIndex = 2
        '
        'lblroomType
        '
        Me.lblroomType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomType.Location = New System.Drawing.Point(7, 50)
        Me.lblroomType.Name = "lblroomType"
        Me.lblroomType.Size = New System.Drawing.Size(89, 20)
        Me.lblroomType.TabIndex = 2
        Me.lblroomType.Text = "Type"
        Me.lblroomType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomPrice
        '
        Me.lblroomPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomPrice.Location = New System.Drawing.Point(7, 79)
        Me.lblroomPrice.Name = "lblroomPrice"
        Me.lblroomPrice.Size = New System.Drawing.Size(89, 20)
        Me.lblroomPrice.TabIndex = 4
        Me.lblroomPrice.Text = "Price"
        Me.lblroomPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomNumberOfBeds
        '
        Me.lblroomNumberOfBeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomNumberOfBeds.Location = New System.Drawing.Point(7, 108)
        Me.lblroomNumberOfBeds.Name = "lblroomNumberOfBeds"
        Me.lblroomNumberOfBeds.Size = New System.Drawing.Size(89, 20)
        Me.lblroomNumberOfBeds.TabIndex = 6
        Me.lblroomNumberOfBeds.Text = "Number of Beds"
        Me.lblroomNumberOfBeds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomAvailability
        '
        Me.lblroomAvailability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomAvailability.Location = New System.Drawing.Point(7, 138)
        Me.lblroomAvailability.Name = "lblroomAvailability"
        Me.lblroomAvailability.Size = New System.Drawing.Size(89, 20)
        Me.lblroomAvailability.TabIndex = 8
        Me.lblroomAvailability.Text = "Availability"
        Me.lblroomAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomFloor
        '
        Me.lblroomFloor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomFloor.Location = New System.Drawing.Point(7, 168)
        Me.lblroomFloor.Name = "lblroomFloor"
        Me.lblroomFloor.Size = New System.Drawing.Size(89, 20)
        Me.lblroomFloor.TabIndex = 10
        Me.lblroomFloor.Text = "Floor"
        Me.lblroomFloor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomDescription
        '
        Me.lblroomDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomDescription.Location = New System.Drawing.Point(7, 199)
        Me.lblroomDescription.Name = "lblroomDescription"
        Me.lblroomDescription.Size = New System.Drawing.Size(89, 20)
        Me.lblroomDescription.TabIndex = 12
        Me.lblroomDescription.Text = "Description"
        Me.lblroomDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblroomRoomNumber
        '
        Me.lblroomRoomNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblroomRoomNumber.Location = New System.Drawing.Point(7, 20)
        Me.lblroomRoomNumber.Name = "lblroomRoomNumber"
        Me.lblroomRoomNumber.Size = New System.Drawing.Size(89, 20)
        Me.lblroomRoomNumber.TabIndex = 0
        Me.lblroomRoomNumber.Text = "Room Number"
        Me.lblroomRoomNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(963, 148)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 175
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'picErrorField7
        '
        Me.picErrorField7.Image = CType(resources.GetObject("picErrorField7.Image"), System.Drawing.Image)
        Me.picErrorField7.Location = New System.Drawing.Point(963, 327)
        Me.picErrorField7.Name = "picErrorField7"
        Me.picErrorField7.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField7.TabIndex = 181
        Me.picErrorField7.TabStop = False
        Me.picErrorField7.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(963, 296)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 180
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(963, 266)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 179
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(963, 236)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 178
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(963, 208)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 177
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(963, 179)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 176
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(539, 61)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 182
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'FindRoom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1035, 465)
        Me.Controls.Add(Me.picErrorField)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.picErrorField7)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.gbxRoom)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btntDelete)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnfindroom)
        Me.Controls.Add(Me.txtfindroomvalue)
        Me.Controls.Add(Me.cbbFind)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblFindRoom)
        Me.Name = "FindRoom"
        Me.Text = "FindRoom"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxRoom.ResumeLayout(False)
        Me.gbxRoom.PerformLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnClose As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnPrev As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dgvSearchResult As DataGridView
    Friend WithEvents btnfindroom As Button
    Friend WithEvents txtfindroomvalue As TextBox
    Friend WithEvents cbbFind As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents lblFindRoom As Label
    Friend WithEvents gbxRoom As GroupBox
    Friend WithEvents txtFloor As TextBox
    Friend WithEvents txtNumberOfBeds As TextBox
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtRoomNumber As TextBox
    Friend WithEvents radiobtnUnavailable As RadioButton
    Friend WithEvents radiobtnAvailable As RadioButton
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents txtType As TextBox
    Friend WithEvents lblroomType As Label
    Friend WithEvents lblroomPrice As Label
    Friend WithEvents lblroomNumberOfBeds As Label
    Friend WithEvents lblroomAvailability As Label
    Friend WithEvents lblroomFloor As Label
    Friend WithEvents lblroomDescription As Label
    Friend WithEvents lblroomRoomNumber As Label
    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents picErrorField7 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents picErrorField As PictureBox
End Class
