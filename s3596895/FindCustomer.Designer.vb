﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FindCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FindCustomer))
        Me.lblFindcustomer = New System.Windows.Forms.Label()
        Me.picErrorField = New System.Windows.Forms.PictureBox()
        Me.btnfindcustomer = New System.Windows.Forms.Button()
        Me.txtfindcustomervalue = New System.Windows.Forms.TextBox()
        Me.cbbFind = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgvSearchResult = New System.Windows.Forms.DataGridView()
        Me.txttitle = New System.Windows.Forms.TextBox()
        Me.lbltitle = New System.Windows.Forms.Label()
        Me.txtcustomerid = New System.Windows.Forms.TextBox()
        Me.lblcustomerid = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.DobValue = New System.Windows.Forms.DateTimePicker()
        Me.radiobtnFemale = New System.Windows.Forms.RadioButton()
        Me.radiobtnMale = New System.Windows.Forms.RadioButton()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.lblcusLastName = New System.Windows.Forms.Label()
        Me.lblcusPhone = New System.Windows.Forms.Label()
        Me.lblcusGender = New System.Windows.Forms.Label()
        Me.gbxCustomer = New System.Windows.Forms.GroupBox()
        Me.txtdob = New System.Windows.Forms.TextBox()
        Me.lblcusAddress = New System.Windows.Forms.Label()
        Me.lblcusEmail = New System.Windows.Forms.Label()
        Me.lblcusDob = New System.Windows.Forms.Label()
        Me.lblcusFirstName = New System.Windows.Forms.Label()
        Me.picErrorField1 = New System.Windows.Forms.PictureBox()
        Me.picErrorField6 = New System.Windows.Forms.PictureBox()
        Me.picErrorField3 = New System.Windows.Forms.PictureBox()
        Me.picErrorField2 = New System.Windows.Forms.PictureBox()
        Me.picErrorField5 = New System.Windows.Forms.PictureBox()
        Me.picErrorField4 = New System.Windows.Forms.PictureBox()
        Me.btntDelete = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnreport = New System.Windows.Forms.Button()
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxCustomer.SuspendLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFindcustomer
        '
        Me.lblFindcustomer.AutoSize = True
        Me.lblFindcustomer.Font = New System.Drawing.Font("Calibri", 30.0!, System.Drawing.FontStyle.Bold)
        Me.lblFindcustomer.Location = New System.Drawing.Point(441, 8)
        Me.lblFindcustomer.Name = "lblFindcustomer"
        Me.lblFindcustomer.Size = New System.Drawing.Size(262, 49)
        Me.lblFindcustomer.TabIndex = 115
        Me.lblFindcustomer.Text = "Find Customer"
        '
        'picErrorField
        '
        Me.picErrorField.Image = CType(resources.GetObject("picErrorField.Image"), System.Drawing.Image)
        Me.picErrorField.Location = New System.Drawing.Point(468, 61)
        Me.picErrorField.Name = "picErrorField"
        Me.picErrorField.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField.TabIndex = 145
        Me.picErrorField.TabStop = False
        Me.picErrorField.Visible = False
        '
        'btnfindcustomer
        '
        Me.btnfindcustomer.Location = New System.Drawing.Point(494, 61)
        Me.btnfindcustomer.Name = "btnfindcustomer"
        Me.btnfindcustomer.Size = New System.Drawing.Size(75, 21)
        Me.btnfindcustomer.TabIndex = 144
        Me.btnfindcustomer.Text = "Find"
        Me.btnfindcustomer.UseVisualStyleBackColor = True
        '
        'txtfindcustomervalue
        '
        Me.txtfindcustomervalue.Enabled = False
        Me.txtfindcustomervalue.Location = New System.Drawing.Point(244, 60)
        Me.txtfindcustomervalue.Name = "txtfindcustomervalue"
        Me.txtfindcustomervalue.Size = New System.Drawing.Size(218, 20)
        Me.txtfindcustomervalue.TabIndex = 143
        '
        'cbbFind
        '
        Me.cbbFind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFind.Location = New System.Drawing.Point(113, 61)
        Me.cbbFind.Name = "cbbFind"
        Me.cbbFind.Size = New System.Drawing.Size(125, 21)
        Me.cbbFind.TabIndex = 142
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.Label4.Location = New System.Drawing.Point(17, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 32)
        Me.Label4.TabIndex = 141
        Me.Label4.Text = "Find"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(192, 102)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 23)
        Me.Label3.TabIndex = 147
        Me.Label3.Text = "Search result:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvSearchResult)
        Me.Panel1.Location = New System.Drawing.Point(7, 114)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 302)
        Me.Panel1.TabIndex = 146
        '
        'dgvSearchResult
        '
        Me.dgvSearchResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSearchResult.Location = New System.Drawing.Point(0, 14)
        Me.dgvSearchResult.Name = "dgvSearchResult"
        Me.dgvSearchResult.ReadOnly = True
        Me.dgvSearchResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSearchResult.Size = New System.Drawing.Size(474, 285)
        Me.dgvSearchResult.TabIndex = 0
        '
        'txttitle
        '
        Me.txttitle.Enabled = False
        Me.txttitle.Location = New System.Drawing.Point(80, 261)
        Me.txttitle.Name = "txttitle"
        Me.txttitle.Size = New System.Drawing.Size(258, 20)
        Me.txttitle.TabIndex = 19
        '
        'lbltitle
        '
        Me.lbltitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbltitle.Location = New System.Drawing.Point(6, 260)
        Me.lbltitle.Name = "lbltitle"
        Me.lbltitle.Size = New System.Drawing.Size(68, 20)
        Me.lbltitle.TabIndex = 18
        Me.lbltitle.Text = "Title"
        Me.lbltitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtcustomerid
        '
        Me.txtcustomerid.Enabled = False
        Me.txtcustomerid.Location = New System.Drawing.Point(81, 229)
        Me.txtcustomerid.Name = "txtcustomerid"
        Me.txtcustomerid.Size = New System.Drawing.Size(258, 20)
        Me.txtcustomerid.TabIndex = 17
        '
        'lblcustomerid
        '
        Me.lblcustomerid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcustomerid.Location = New System.Drawing.Point(5, 230)
        Me.lblcustomerid.Name = "lblcustomerid"
        Me.lblcustomerid.Size = New System.Drawing.Size(68, 20)
        Me.lblcustomerid.TabIndex = 16
        Me.lblcustomerid.Text = "Customer ID"
        Me.lblcustomerid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtPhone
        '
        Me.txtPhone.Enabled = False
        Me.txtPhone.Location = New System.Drawing.Point(81, 109)
        Me.txtPhone.MaxLength = 9
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(257, 20)
        Me.txtPhone.TabIndex = 3
        '
        'DobValue
        '
        Me.DobValue.Location = New System.Drawing.Point(80, 200)
        Me.DobValue.Name = "DobValue"
        Me.DobValue.Size = New System.Drawing.Size(178, 20)
        Me.DobValue.TabIndex = 6
        Me.DobValue.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        Me.DobValue.Visible = False
        '
        'radiobtnFemale
        '
        Me.radiobtnFemale.AutoSize = True
        Me.radiobtnFemale.Enabled = False
        Me.radiobtnFemale.Location = New System.Drawing.Point(279, 22)
        Me.radiobtnFemale.Name = "radiobtnFemale"
        Me.radiobtnFemale.Size = New System.Drawing.Size(59, 17)
        Me.radiobtnFemale.TabIndex = 6
        Me.radiobtnFemale.Text = "Female"
        Me.radiobtnFemale.UseVisualStyleBackColor = True
        '
        'radiobtnMale
        '
        Me.radiobtnMale.AutoSize = True
        Me.radiobtnMale.Enabled = False
        Me.radiobtnMale.Location = New System.Drawing.Point(80, 22)
        Me.radiobtnMale.Name = "radiobtnMale"
        Me.radiobtnMale.Size = New System.Drawing.Size(48, 17)
        Me.radiobtnMale.TabIndex = 5
        Me.radiobtnMale.Text = "Male"
        Me.radiobtnMale.UseVisualStyleBackColor = True
        '
        'txtEmail
        '
        Me.txtEmail.Enabled = False
        Me.txtEmail.Location = New System.Drawing.Point(80, 169)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(258, 20)
        Me.txtEmail.TabIndex = 5
        '
        'txtAddress
        '
        Me.txtAddress.Enabled = False
        Me.txtAddress.Location = New System.Drawing.Point(80, 140)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(258, 20)
        Me.txtAddress.TabIndex = 4
        '
        'txtFirstName
        '
        Me.txtFirstName.Enabled = False
        Me.txtFirstName.Location = New System.Drawing.Point(80, 51)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(258, 20)
        Me.txtFirstName.TabIndex = 1
        '
        'txtLastName
        '
        Me.txtLastName.Enabled = False
        Me.txtLastName.Location = New System.Drawing.Point(80, 80)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(258, 20)
        Me.txtLastName.TabIndex = 2
        '
        'lblcusLastName
        '
        Me.lblcusLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusLastName.Location = New System.Drawing.Point(6, 80)
        Me.lblcusLastName.Name = "lblcusLastName"
        Me.lblcusLastName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusLastName.TabIndex = 7
        Me.lblcusLastName.Text = "Last Name"
        Me.lblcusLastName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusPhone
        '
        Me.lblcusPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusPhone.Location = New System.Drawing.Point(6, 110)
        Me.lblcusPhone.Name = "lblcusPhone"
        Me.lblcusPhone.Size = New System.Drawing.Size(68, 20)
        Me.lblcusPhone.TabIndex = 9
        Me.lblcusPhone.Text = "Phone"
        Me.lblcusPhone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusGender
        '
        Me.lblcusGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusGender.Location = New System.Drawing.Point(6, 20)
        Me.lblcusGender.Name = "lblcusGender"
        Me.lblcusGender.Size = New System.Drawing.Size(68, 20)
        Me.lblcusGender.TabIndex = 2
        Me.lblcusGender.Text = "Gender"
        Me.lblcusGender.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbxCustomer
        '
        Me.gbxCustomer.Controls.Add(Me.txtdob)
        Me.gbxCustomer.Controls.Add(Me.txttitle)
        Me.gbxCustomer.Controls.Add(Me.lbltitle)
        Me.gbxCustomer.Controls.Add(Me.txtcustomerid)
        Me.gbxCustomer.Controls.Add(Me.lblcustomerid)
        Me.gbxCustomer.Controls.Add(Me.txtPhone)
        Me.gbxCustomer.Controls.Add(Me.DobValue)
        Me.gbxCustomer.Controls.Add(Me.radiobtnFemale)
        Me.gbxCustomer.Controls.Add(Me.radiobtnMale)
        Me.gbxCustomer.Controls.Add(Me.txtEmail)
        Me.gbxCustomer.Controls.Add(Me.txtAddress)
        Me.gbxCustomer.Controls.Add(Me.txtFirstName)
        Me.gbxCustomer.Controls.Add(Me.txtLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusLastName)
        Me.gbxCustomer.Controls.Add(Me.lblcusPhone)
        Me.gbxCustomer.Controls.Add(Me.lblcusAddress)
        Me.gbxCustomer.Controls.Add(Me.lblcusEmail)
        Me.gbxCustomer.Controls.Add(Me.lblcusDob)
        Me.gbxCustomer.Controls.Add(Me.lblcusFirstName)
        Me.gbxCustomer.Controls.Add(Me.lblcusGender)
        Me.gbxCustomer.Location = New System.Drawing.Point(541, 114)
        Me.gbxCustomer.Name = "gbxCustomer"
        Me.gbxCustomer.Size = New System.Drawing.Size(345, 299)
        Me.gbxCustomer.TabIndex = 148
        Me.gbxCustomer.TabStop = False
        Me.gbxCustomer.Text = "Customer Details"
        '
        'txtdob
        '
        Me.txtdob.Enabled = False
        Me.txtdob.Location = New System.Drawing.Point(81, 201)
        Me.txtdob.Name = "txtdob"
        Me.txtdob.Size = New System.Drawing.Size(259, 20)
        Me.txtdob.TabIndex = 157
        '
        'lblcusAddress
        '
        Me.lblcusAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusAddress.Location = New System.Drawing.Point(6, 140)
        Me.lblcusAddress.Name = "lblcusAddress"
        Me.lblcusAddress.Size = New System.Drawing.Size(68, 20)
        Me.lblcusAddress.TabIndex = 11
        Me.lblcusAddress.Text = "Address"
        Me.lblcusAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusEmail
        '
        Me.lblcusEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusEmail.Location = New System.Drawing.Point(6, 169)
        Me.lblcusEmail.Name = "lblcusEmail"
        Me.lblcusEmail.Size = New System.Drawing.Size(68, 20)
        Me.lblcusEmail.TabIndex = 13
        Me.lblcusEmail.Text = "Email"
        Me.lblcusEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusDob
        '
        Me.lblcusDob.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusDob.Location = New System.Drawing.Point(5, 200)
        Me.lblcusDob.Name = "lblcusDob"
        Me.lblcusDob.Size = New System.Drawing.Size(68, 20)
        Me.lblcusDob.TabIndex = 15
        Me.lblcusDob.Text = "Dob"
        Me.lblcusDob.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblcusFirstName
        '
        Me.lblcusFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblcusFirstName.Location = New System.Drawing.Point(6, 51)
        Me.lblcusFirstName.Name = "lblcusFirstName"
        Me.lblcusFirstName.Size = New System.Drawing.Size(68, 20)
        Me.lblcusFirstName.TabIndex = 5
        Me.lblcusFirstName.Text = "First Name"
        Me.lblcusFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picErrorField1
        '
        Me.picErrorField1.Image = CType(resources.GetObject("picErrorField1.Image"), System.Drawing.Image)
        Me.picErrorField1.Location = New System.Drawing.Point(893, 134)
        Me.picErrorField1.Name = "picErrorField1"
        Me.picErrorField1.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField1.TabIndex = 149
        Me.picErrorField1.TabStop = False
        Me.picErrorField1.Visible = False
        '
        'picErrorField6
        '
        Me.picErrorField6.Image = CType(resources.GetObject("picErrorField6.Image"), System.Drawing.Image)
        Me.picErrorField6.Location = New System.Drawing.Point(892, 284)
        Me.picErrorField6.Name = "picErrorField6"
        Me.picErrorField6.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField6.TabIndex = 154
        Me.picErrorField6.TabStop = False
        Me.picErrorField6.Visible = False
        '
        'picErrorField3
        '
        Me.picErrorField3.Image = CType(resources.GetObject("picErrorField3.Image"), System.Drawing.Image)
        Me.picErrorField3.Location = New System.Drawing.Point(893, 195)
        Me.picErrorField3.Name = "picErrorField3"
        Me.picErrorField3.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField3.TabIndex = 152
        Me.picErrorField3.TabStop = False
        Me.picErrorField3.Visible = False
        '
        'picErrorField2
        '
        Me.picErrorField2.Image = CType(resources.GetObject("picErrorField2.Image"), System.Drawing.Image)
        Me.picErrorField2.Location = New System.Drawing.Point(893, 164)
        Me.picErrorField2.Name = "picErrorField2"
        Me.picErrorField2.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField2.TabIndex = 151
        Me.picErrorField2.TabStop = False
        Me.picErrorField2.Visible = False
        '
        'picErrorField5
        '
        Me.picErrorField5.Image = CType(resources.GetObject("picErrorField5.Image"), System.Drawing.Image)
        Me.picErrorField5.Location = New System.Drawing.Point(893, 254)
        Me.picErrorField5.Name = "picErrorField5"
        Me.picErrorField5.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField5.TabIndex = 150
        Me.picErrorField5.TabStop = False
        Me.picErrorField5.Visible = False
        '
        'picErrorField4
        '
        Me.picErrorField4.Image = CType(resources.GetObject("picErrorField4.Image"), System.Drawing.Image)
        Me.picErrorField4.Location = New System.Drawing.Point(892, 224)
        Me.picErrorField4.Name = "picErrorField4"
        Me.picErrorField4.Size = New System.Drawing.Size(20, 20)
        Me.picErrorField4.TabIndex = 153
        Me.picErrorField4.TabStop = False
        Me.picErrorField4.Visible = False
        '
        'btntDelete
        '
        Me.btntDelete.Location = New System.Drawing.Point(547, 430)
        Me.btntDelete.Name = "btntDelete"
        Me.btntDelete.Size = New System.Drawing.Size(75, 23)
        Me.btntDelete.TabIndex = 156
        Me.btntDelete.Text = "Delete"
        Me.btntDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(811, 430)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.TabIndex = 155
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(724, 430)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 158
        Me.btnNext.Text = "Next record"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(628, 430)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 23)
        Me.btnPrev.TabIndex = 157
        Me.btnPrev.Text = "Previous record"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(376, 426)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 30)
        Me.btnClose.TabIndex = 159
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnreport
        '
        Me.btnreport.Location = New System.Drawing.Point(893, 393)
        Me.btnreport.Name = "btnreport"
        Me.btnreport.Size = New System.Drawing.Size(75, 23)
        Me.btnreport.TabIndex = 160
        Me.btnreport.Text = "Report"
        Me.btnreport.UseVisualStyleBackColor = True
        '
        'FindCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1035, 465)
        Me.Controls.Add(Me.btnreport)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btntDelete)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.gbxCustomer)
        Me.Controls.Add(Me.picErrorField1)
        Me.Controls.Add(Me.picErrorField6)
        Me.Controls.Add(Me.picErrorField3)
        Me.Controls.Add(Me.picErrorField2)
        Me.Controls.Add(Me.picErrorField5)
        Me.Controls.Add(Me.picErrorField4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.picErrorField)
        Me.Controls.Add(Me.btnfindcustomer)
        Me.Controls.Add(Me.txtfindcustomervalue)
        Me.Controls.Add(Me.cbbFind)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblFindcustomer)
        Me.Name = "FindCustomer"
        Me.Text = "FindCustomer"
        CType(Me.picErrorField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvSearchResult, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxCustomer.ResumeLayout(False)
        Me.gbxCustomer.PerformLayout()
        CType(Me.picErrorField1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorField4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblFindcustomer As Label
    Friend WithEvents picErrorField As PictureBox
    Friend WithEvents btnfindcustomer As Button
    Friend WithEvents txtfindcustomervalue As TextBox
    Friend WithEvents cbbFind As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dgvSearchResult As DataGridView
    Friend WithEvents txttitle As TextBox
    Friend WithEvents lbltitle As Label
    Friend WithEvents txtcustomerid As TextBox
    Friend WithEvents lblcustomerid As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents DobValue As DateTimePicker
    Friend WithEvents radiobtnFemale As RadioButton
    Friend WithEvents radiobtnMale As RadioButton
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtFirstName As TextBox
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents lblcusLastName As Label
    Friend WithEvents lblcusPhone As Label
    Friend WithEvents lblcusGender As Label
    Friend WithEvents gbxCustomer As GroupBox
    Friend WithEvents lblcusAddress As Label
    Friend WithEvents lblcusEmail As Label
    Friend WithEvents lblcusDob As Label
    Friend WithEvents lblcusFirstName As Label
    Friend WithEvents picErrorField1 As PictureBox
    Friend WithEvents picErrorField6 As PictureBox
    Friend WithEvents picErrorField3 As PictureBox
    Friend WithEvents picErrorField2 As PictureBox
    Friend WithEvents picErrorField5 As PictureBox
    Friend WithEvents picErrorField4 As PictureBox
    Friend WithEvents btntDelete As Button
    Friend WithEvents btnEdit As Button
    Friend WithEvents txtdob As TextBox
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPrev As Button
    Friend WithEvents btnClose As Button
    Friend WithEvents btnreport As Button
End Class
